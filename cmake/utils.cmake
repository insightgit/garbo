function(setup_diff)
	if(WIN32)
		if(NOT EXISTS ${CMAKE_BINARY_DIR}/bin/diff.exe)
			file(ARCHIVE_EXTRACT INPUT ${CMAKE_SOURCE_DIR}/cmake/tools/diffutils-windows.zip DESTINATION ${CMAKE_BINARY_DIR})
		endif()

		# TODO(Bobby): implement some sort of test running
		set(DIFF ${CMAKE_BINARY_DIR}/bin/diff.exe PARENT_SCOPE)
	else()
		find_program(DIFF diff)
	endif()
endfunction()

function(add_ctest_test test_file)
	if(WIN32)
        find_program(PYTHON3 python)
    else()
        find_program(PYTHON3 python3)
    endif()

	setup_diff()

	get_filename_component(test_name ${test_file} NAME_WLE)
	get_filename_component(test_file_last_extension ${test_file} LAST_EXT)
	string(REPLACE ${test_file_last_extension} "" test_file_no_last_extension ${test_file})


	add_executable(${test_name} ${test_file})
	target_link_libraries(${test_name} Garbo)

	set_property(TARGET ${test_name} PROPERTY CXX_STANDARD 20)

	add_test(NAME test-${test_name}
			 COMMAND ${PYTHON3} ${CMAKE_SOURCE_DIR}/cmake/tools/test_runner.py ${CMAKE_BINARY_DIR}/${test_name}.exe ${CMAKE_BINARY_DIR}/${test_name}.raw ${DIFF} ${test_file_no_last_extension}.ok)
endfunction()