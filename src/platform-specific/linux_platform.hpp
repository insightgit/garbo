//
// Created by bobby on 12/07/23.
//

#ifndef GARBO_LINUX_PLATFORM_HPP
#define GARBO_LINUX_PLATFORM_HPP

#include <cstdint>
#include <stdexcept>
#include <utility>

#include <pthread.h>

extern "C" {
    void save_reg_state(uint64_t* reg_state);
}

inline std::pair<uint64_t*, uint64_t*> get_thread_stack_info() {
    pthread_attr_t attrs;
    if(pthread_getattr_np( pthread_self(), &attrs ) != 0) {
        throw std::runtime_error("Couldn't get thread attributes!");
    }
    void   *stack_addr;
    size_t  stack_size;

    if(pthread_attr_getstack( &attrs, &stack_addr, &stack_size ) != 0) {
        throw std::runtime_error("Couldn't get stack attributes from thread attributes!");
    }

    uintptr_t *stack_bottom = reinterpret_cast<uintptr_t*>(reinterpret_cast<uintptr_t>(stack_addr) + stack_size);
    uintptr_t *stack_top = reinterpret_cast<uintptr_t*>(stack_addr);

    return { stack_bottom, stack_top };
}

#endif //GARBO_LINUX_PLATFORM_HPP
