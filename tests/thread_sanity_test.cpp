#include <exception>
#include <iostream>
#include <thread>

#include "../src/garbage_collector.hpp"

int main() {
	garbo::GarbageCollector garbage_collector{true};
	const char *test_message = "hey now\0";
	const char *overwritten_message = "overwri\0";
	bool thread_should_stay_alive = true;

	std::thread my_saved_message_thread{ [&garbage_collector, &thread_should_stay_alive, test_message]() {
		char* saved_message = reinterpret_cast<char*>(garbage_collector.GC_malloc(8));
		
		strcpy(saved_message, test_message);

		while (thread_should_stay_alive) {};

		if (strncmp(saved_message, test_message, 8) == 0) {
			std::cout << "*** saved message is intact!\n";
		}
		else {
			throw std::runtime_error("*** saved message is not intact! there was a memory corruption!");
		}
	}};

	std::array<char*, 100> allocs;

	for (int i = 0; 100 > i; ++i) {
		allocs[i] = reinterpret_cast<char*>(garbage_collector.GC_malloc(8));

		garbage_collector.force_garbage_search();

		if (allocs[i] == nullptr) {
			thread_should_stay_alive = false;
			my_saved_message_thread.join();

			throw std::runtime_error("*** test string is null! alloc failed!");
		}

		strcpy(allocs[i], overwritten_message);
	}

	garbage_collector.force_garbage_search();

	thread_should_stay_alive = false;
	my_saved_message_thread.join();

	std::cout << "garbage collector used bytes " << garbage_collector.get_used_bytes() << "\n";
}