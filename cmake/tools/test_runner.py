#!/usr/bin/env python3

import os
import subprocess
import sys

import terminal_colors

CATCH_PREFIX = "***"

def check_env_then_set(env_var, default):
    return os.environ[env_var] if env_var in os.environ else default

def main():
    if len(sys.argv) <= 4:
        print(f"args: {len(sys.argv)}")
        print(f"Usage: {sys.argv[0]} program_path raw_output_file diff_program ok_file")
        sys.exit(2)

    pass_count = 0
    pass_timeout_count = 0
    failed_count = 0
    failed_timeout_count = 0

    high_time = 0.0
    low_time = 100000.0
    time_sum = 0.0
    loop_limit = 1
    timeout_time = 10

    raw_output_file = sys.argv[2]
    filtered_output_file_path = raw_output_file.replace(".raw", ".out")

    if os.path.exists(raw_output_file):
        os.remove(raw_output_file)

    if os.path.exists(filtered_output_file_path):
        os.remove(filtered_output_file_path)

    for i in range(int(loop_limit)):
        qemu_process_array = [sys.argv[1]]

        #{terminal_colors.blue}
        print(f"Running test")

        timeout = False
        test_time = -1.0

        with open(raw_output_file, 'w') as raw_output_file_write:
            try:
                subprocess.run(qemu_process_array, timeout=float(timeout_time), stdout=raw_output_file_write, stderr=raw_output_file_write)
            except subprocess.TimeoutExpired:
                timeout = True

        test_time_string = f"{round(test_time, 3)}s"

        with open(sys.argv[2], 'r') as raw_output_file:
            with open(filtered_output_file_path, 'w') as filtered_output_file:
                for raw_output_line in raw_output_file.readlines():
                    if raw_output_line.startswith(CATCH_PREFIX):
                        filtered_output_file.write(raw_output_line)

        diff_process = subprocess.run([sys.argv[3], filtered_output_file_path, sys.argv[4]], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if diff_process.returncode != 0:
            new_diff = sys.argv[2].replace(".raw", ".diff")

            if timeout:
                # terminal_colors.red 
                print(f"[TIMEOUT] FAILURE: Test failed and timed out")
                failed_timeout_count += 1
            else:
                print("Test FAILED")
                failed_count += 1

            with open(new_diff, 'wb') as diff_file:
                diff_file.write(diff_process.stdout)

            #if end_condition == "fail":
                #break
        elif timeout:
            # terminal_colors.yellow
            print("[TIMEOUT] Test succeeded but timed out")
            pass_timeout_count += 1
        else:
            # terminal_colors.green
            print(f"Test succeeded")
            pass_count += 1

    total_count = pass_count + pass_timeout_count + failed_count + failed_timeout_count

    print("=========================================================")
    print(f"With a timeout of {timeout}s:")
    #terminal_colors.green +
    print(f"Pass: {pass_count}/{total_count}")
    #terminal_colors.yellow + 
    print(f"Pass timeout: {pass_timeout_count}/{total_count}")
    #terminal_colors.red +
    print(f"Failed: {failed_count}/{total_count}")
    print(f"Failed with timeout: {failed_timeout_count}/{total_count}")
    print(f"Max non-timeout time: {round(high_time, 3)}s")
    #print(f"Average time: {round((time_sum / total_count), 3)}s")
    print(f"Min time: {round(low_time, 3)}s")

    if failed_count + failed_timeout_count > 0:
        sys.exit(-2)

if __name__ == "__main__":
	main()