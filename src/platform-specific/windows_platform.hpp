#include <array>
#include <cstdint>

#include <windows.h>
#include <winnt.h>
#include <tlhelp32.h>

#include "../utils.hpp"

extern "C" {
    // uint64_t instead of std::array because it's easier to handle in asm
	void _save_reg_state(uint64_t* reg_state);
}

inline std::unordered_set<DWORD> get_thread_ids() {
    DWORD current_process_id = GetCurrentProcessId();
    std::unordered_set<DWORD> thread_ids;

    HANDLE toolhelp_handle = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, current_process_id);
    if (toolhelp_handle != INVALID_HANDLE_VALUE) {
        THREADENTRY32 thread_entry;
        thread_entry.dwSize = sizeof(thread_entry);
        if (Thread32First(toolhelp_handle, &thread_entry)) {
            do {
                if (thread_entry.dwSize >= FIELD_OFFSET(THREADENTRY32, th32OwnerProcessID) +
                    sizeof(thread_entry.th32OwnerProcessID) && thread_entry.th32OwnerProcessID == current_process_id) {
                    thread_ids.insert(thread_entry.th32ThreadID);
                }
                thread_entry.dwSize = sizeof(thread_entry);
            } while (Thread32Next(toolhelp_handle, &thread_entry));
        }
        CloseHandle(toolhelp_handle);
    }

    return thread_ids;
}

inline void get_thread_reg_state(std::optional<DWORD> thread_id, garbo::RegState &reg_state) {
    if (thread_id.has_value() && GetCurrentThreadId() != thread_id.value()) {
        HANDLE thread_handle = OpenThread(THREAD_ALL_ACCESS, FALSE, thread_id.value());
        if (thread_handle == nullptr) {
            throw std::runtime_error("Failed to open thread. Is thread id valid?");
        }

        CONTEXT context;
        context.ContextFlags = CONTEXT_CONTROL | CONTEXT_INTEGER;

        if (GetThreadContext(thread_handle, &context)) {
            reg_state[0] = context.Rcx;
            reg_state[1] = context.Rbx;
            reg_state[2] = context.Rdx;
            reg_state[3] = context.Rsi;
            reg_state[4] = context.Rdi;
            reg_state[5] = context.Rbp;
            reg_state[6] = context.Rsp;
            reg_state[7] = context.R8;
            reg_state[8] = context.R9;
            reg_state[9] = context.R10;
            reg_state[10] = context.R11;
            reg_state[11] = context.R12;
            reg_state[12] = context.R13;
            reg_state[13] = context.R14;
            reg_state[14] = context.R15;
        }

        CloseHandle(thread_handle);
    }
    else {
        _save_reg_state(reg_state.data());
    }
}

// if thread is optional, we'll get the stack state for the current thread
inline std::pair<uint64_t*, uint64_t*> get_thread_stack_info(std::optional<DWORD> thread_id) {
	if (thread_id.has_value() && GetCurrentThreadId() != thread_id.value()) {
        // Hopefully this cleaner ChatGPT code works

        HANDLE thread_handle = OpenThread(THREAD_ALL_ACCESS, FALSE, thread_id.value());
        if (thread_handle == nullptr) {
            throw std::runtime_error("Failed to open thread. Is thread id valid?");
        }

        CONTEXT context;
        context.ContextFlags = CONTEXT_CONTROL;

        if (GetThreadContext(thread_handle, &context)) {
            // On x86_64, the stack base is stored in the RSP (stack pointer) register.
            // The stack size can be determined by querying the memory regions using VirtualQueryEx.
            void *stack_base = reinterpret_cast<void*>(context.Rsp);

            MEMORY_BASIC_INFORMATION mbi;
            if (VirtualQuery(stack_base, &mbi, sizeof(mbi)) != 0) {
                uintptr_t *stack_limit = reinterpret_cast<uintptr_t*>(mbi.RegionSize + reinterpret_cast<uintptr_t>(mbi.BaseAddress));
                
                return { stack_limit, reinterpret_cast<uintptr_t*>(mbi.BaseAddress)};
            }
            else {
                throw std::runtime_error("Failed to query stack information");
            }
        }
        else {
            throw std::runtime_error("Failed to get thread context");
        }

        CloseHandle(thread_handle);
	}
	else {
        // win64 magic incantation from Thomas Johnstone here: https://stackoverflow.com/a/11366101
		NT_TIB* tib = reinterpret_cast<NT_TIB*>(__readgsqword(0x30));
		uintptr_t* stack_bottom = (uintptr_t*)tib->StackLimit;
		uintptr_t* stack_top = (uintptr_t*)tib->StackBase;

		return { stack_top, stack_bottom };
	}
}