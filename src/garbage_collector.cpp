#include "garbage_collector.hpp"

garbo::GarbageCollector::GarbageCollector(bool zero_memory_on_sweep, uintptr_t initial_arena_size, std::optional<uintptr_t> max_arena_size, bool continuous_search_on_dedicated_thread) : m_zero_memory_on_sweep(zero_memory_on_sweep), 
																																														  m_max_arena_size(max_arena_size) {
	initial_arena_size += initial_arena_size % sizeof(uintptr_t);

	if (m_max_arena_size.has_value()) {
		m_max_arena_size = m_max_arena_size.value() + (m_max_arena_size.value() % sizeof(uintptr_t));
	}

	if (continuous_search_on_dedicated_thread) {
		m_garbage_searcher_thread = std::thread([this] {
			// this is bad, ideally optimize this so its just not busylooping when we have nothing to search for
			while (!m_destructing_object) {
				std::unordered_map<uintptr_t, uintptr_t> allocations_copy;

				{
					// we can't have m_allocations changing while we are copying it!
					std::lock_guard<std::mutex> garbage_collector_lock_guard{m_garbage_collector_lock};
					allocations_copy = m_allocations;
				}

				find_garbage(allocations_copy);
			}
		});
		m_garbage_collector_thread = std::thread([this] {
			while (!m_destructing_object) {
				std::this_thread::sleep_for(std::chrono::milliseconds(GARBAGE_COLLECTOR_THREAD_INTERVAL_MILLIS));

				check_for_garbage();
			}
		});
	}
}

std::optional<uintptr_t*> garbo::GarbageCollector::mark_and_use_free_allocation(const std::pair<uintptr_t, uintptr_t> &free_allocation, size_t size_plus_overhead, size_t size) {
	if (free_allocation.second == size_plus_overhead) {
		m_allocations.insert(free_allocation);
	}
	else if (free_allocation.second > size_plus_overhead) {
		std::pair<uintptr_t, uintptr_t> spare_space_free_allocation{ free_allocation.first + size_plus_overhead, free_allocation.second - size_plus_overhead };
		std::pair<uintptr_t, uintptr_t> new_allocation {free_allocation.first, size_plus_overhead};

		uintptr_t* spare_space_free_allocation_ptr = reinterpret_cast<uintptr_t*>(spare_space_free_allocation.first);

		// check sanity bit of old allocation, just to be safe
		uintptr_t sanity_bit = spare_space_free_allocation_ptr[(spare_space_free_allocation.second / sizeof(uintptr_t)) - 1];

		assert(sanity_bit >> 63 == 1);

		spare_space_free_allocation_ptr[(spare_space_free_allocation.second / sizeof(uintptr_t)) - 1] = (static_cast<uintptr_t>(1) << 63) | spare_space_free_allocation.second;

		m_freed_allocations.insert(spare_space_free_allocation);
		m_allocations.insert(new_allocation);
	}
	else {
		return {};
	}

	m_freed_allocations.erase(free_allocation.first);

	uintptr_t* new_alloc_ptr = reinterpret_cast<uintptr_t*>(free_allocation.first);

	uintptr_t alloc_ptr_pos = (size_plus_overhead / sizeof(uintptr_t)) - 1;

	assert(size_plus_overhead % sizeof(uintptr_t) == 0);

	new_alloc_ptr[alloc_ptr_pos] = (static_cast<uintptr_t>(1) << 63) | size;

	return new_alloc_ptr;
}

void* garbo::GarbageCollector::GC_malloc(uintptr_t size) {
	if (size == 0) {
		return nullptr;
	}

	// one uintptr_t at the back of the allocation for allocation size
	uintptr_t aligned_size = (size / sizeof(uintptr_t)) * sizeof(uintptr_t);

	if (size % sizeof(uintptr_t) != 0) {
		aligned_size += sizeof(uintptr_t);
	}

	uintptr_t size_plus_overhead = sizeof(uintptr_t) + aligned_size;

	std::lock_guard<std::mutex> garbage_collector_lock_guard{m_garbage_collector_lock};

	#ifndef NDEBUG
		size_t accounted_allocation_size = 0;

		for (const std::pair<uintptr_t, uintptr_t> &allocation : m_allocations) {
			accounted_allocation_size += allocation.second;
		}

		for (const std::pair<uintptr_t, uintptr_t> &free_allocation : m_freed_allocations) {
			accounted_allocation_size += free_allocation.second;
		}

		assert(accounted_allocation_size == m_total_arena_size);
	#endif

	for (std::pair<uintptr_t, uintptr_t> free_allocation : m_freed_allocations) {
		std::optional<uintptr_t*> allocation_to_return = mark_and_use_free_allocation(free_allocation, size_plus_overhead, size);

		if (allocation_to_return.has_value()) {
			return allocation_to_return.value();
		}
	}

	uintptr_t new_block_size = HEAP_SMALL_BLOCK_SIZE > size ? HEAP_SMALL_BLOCK_SIZE : size + (size % HEAP_SMALL_BLOCK_SIZE);
	MemoryArena new_memory_arena;

	if (m_max_arena_size.has_value() && m_max_arena_size.value() < m_total_arena_size + new_block_size) {
		return nullptr;
	}

	m_total_arena_size += new_block_size;

	try {
		new_memory_arena.ptr.reset(new uintptr_t[new_block_size / sizeof(uintptr_t)]);
	}
	catch (std::bad_alloc &bad_alloc_exception) {
		return nullptr;
	}

	new_memory_arena.size = new_block_size;

	// these shouldn't be affected by new_memory_arena's std::move
	uintptr_t memory_arena_ptr = reinterpret_cast<uintptr_t>(new_memory_arena.ptr.get());
	uintptr_t memory_arena_size = new_memory_arena.size;

	if (new_block_size == HEAP_SMALL_BLOCK_SIZE) {
		m_small_memory_arenas.push_back(std::move(new_memory_arena));
	}
	else {
		m_large_memory_arenas.insert({ new_block_size, std::move(new_memory_arena) });
	}

	m_memory_arenas.insert(memory_arena_ptr);

	return mark_and_use_free_allocation(std::pair<uintptr_t, uintptr_t>{ memory_arena_ptr, memory_arena_size }, size_plus_overhead, size).value();
}

void garbo::GarbageCollector::search_for_ptrs_in_range(uintptr_t* visited_ptr, uintptr_t allocation_size, std::unordered_set<uintptr_t> &visited_set, 
													   std::unordered_set<uintptr_t> &unvisited_set, const std::unordered_map<uintptr_t, uintptr_t> &allocations) {
	for (uintptr_t *current_ptr = visited_ptr; (visited_ptr + (allocation_size / sizeof(uintptr_t))) > current_ptr; ++current_ptr) {
		uintptr_t current_ptr_adjusted = *current_ptr;

		if (unvisited_set.contains(current_ptr_adjusted)) {
			unvisited_set.erase(current_ptr_adjusted);
			visited_set.insert(current_ptr_adjusted);

			search_for_ptrs_in_range(reinterpret_cast<uintptr_t*>(*current_ptr), m_allocations.at(current_ptr_adjusted), visited_set, unvisited_set, allocations);
		}
	}
}

void garbo::GarbageCollector::check_ptr_ranges(std::unordered_set<uintptr_t> &visited_set, std::unordered_set<uintptr_t> &unvisited_set,
											   const garbo::RegState &reg_state, const StackInfo &thread_stack_info, 
											   const std::unordered_map<uintptr_t, uintptr_t> &allocations) {
	// TODO(Bobby): Find a way to make this not O(n)
	std::vector<uintptr_t> visited_ptrs;

	for (uintptr_t unvisited_ptr : unvisited_set) {
		uintptr_t unvisited_ptr_adjusted = unvisited_ptr;
		uintptr_t unvisited_ptr_adjusted_end = unvisited_ptr_adjusted + allocations.at(unvisited_ptr);

		for (const uintptr_t& reg : reg_state) {
			if (reg >= unvisited_ptr_adjusted && reg < unvisited_ptr_adjusted_end) {
				visited_ptrs.push_back(unvisited_ptr);
				break;
			}
		}

		if (!unvisited_set.contains(unvisited_ptr)) {
			continue;
		}

		for (uintptr_t* stack_ptr = thread_stack_info.second; thread_stack_info.first > stack_ptr; ++stack_ptr) {
			if (*stack_ptr >= unvisited_ptr_adjusted && *stack_ptr < unvisited_ptr_adjusted_end) {
				visited_ptrs.push_back(unvisited_ptr);
				break;
			}
		}
	}

	for (uintptr_t visited_ptr : visited_ptrs) {
		unvisited_set.erase(visited_ptr);
		visited_set.insert(visited_ptr);
	}
}

void garbo::GarbageCollector::find_garbage(const std::unordered_map<uintptr_t, uintptr_t> &allocations) {
	std::lock_guard<std::mutex> garbage_lock_guard{m_garbage_ptrs_lock};
	garbo::RegState reg_state;
	std::unordered_set<uintptr_t> unvisited_set;
	std::unordered_set<uintptr_t> visited_set;

	for (const std::pair<const uintptr_t, uintptr_t> &allocation : allocations) {
		// once something has entered the garbage set, it will never leave it
		if (!m_garbage_ptrs.contains(allocation.first)) {
			unvisited_set.insert(allocation.first);
		}
	}

	if (unvisited_set.empty()) {
		return;
	}

	// on windows: std::unordered_set<DWORD>
	auto thread_ids = get_thread_ids();

	for (const auto& thread_id : thread_ids) {
		if (thread_id == GetCurrentThreadId()) {
			continue;
		}

		get_thread_reg_state(thread_id, reg_state);
		StackInfo thread_stack_info = get_thread_stack_info(thread_id);

		for (uint64_t& reg : reg_state) {
			if (unvisited_set.find(reg) != unvisited_set.end()) {
				unvisited_set.erase(reg);
				visited_set.insert(reg);
			}
		}

		// start from the bottom of the stack and go to the top of the stack
		for (uintptr_t* stack_ptr = thread_stack_info.second; thread_stack_info.first > stack_ptr; ++stack_ptr) {
			uintptr_t stack_ptr_adjusted = *stack_ptr;

			if (unvisited_set.contains(stack_ptr_adjusted)) {
				unvisited_set.erase(stack_ptr_adjusted);
				visited_set.insert(stack_ptr_adjusted);
			}
		}

		if (!unvisited_set.empty()) {
			// TODO(Bobby): fix this so we are actually searching ptr ranges instead of just the base ptr!!!!
			check_ptr_ranges(visited_set, unvisited_set, reg_state, thread_stack_info, allocations);
		}
		else {
			break;
		}
	}

	for (uintptr_t current_ptr : visited_set) {
		if (unvisited_set.empty()) {
			break;
		}

		uintptr_t allocation_size = allocations.at(current_ptr);

		search_for_ptrs_in_range(reinterpret_cast<uintptr_t*>(current_ptr), allocations.at(current_ptr), visited_set, unvisited_set, allocations);
	}

	for (uintptr_t unvisited_ptr : unvisited_set) {
		m_garbage_ptrs.insert(unvisited_ptr);
	}
}

void garbo::GarbageCollector::check_for_garbage() {
	std::lock_guard<std::mutex> garbage_lock_guard{m_garbage_ptrs_lock};

	for (uintptr_t unallocated_ptr : m_garbage_ptrs) {
		sweep(unallocated_ptr);
	}

	m_garbage_ptrs.clear();
}

std::optional<std::pair<uintptr_t, uintptr_t>> garbo::GarbageCollector::consolidate_left_if_needed(uintptr_t *current_alloc, uintptr_t current_alloc_size) {
	assert(reinterpret_cast<uintptr_t>(current_alloc) % sizeof(uintptr_t) == 0);

	if (m_memory_arenas.find(reinterpret_cast<uintptr_t>(current_alloc)) != m_memory_arenas.end()) {
		return {};
	}

	uintptr_t left_alloc_size = current_alloc[-1];

	// sanity bit (we are never going to have an alloc greater than 2^63 bytes anyways, right?)
	assert(left_alloc_size >> 63 == 1);

	left_alloc_size &= ~0x8000000000000000;

	uintptr_t* left_alloc_ptr = reinterpret_cast<uintptr_t*>(current_alloc - left_alloc_size);
	uintptr_t left_alloc_adjusted = *left_alloc_ptr;

	if (m_freed_allocations.contains(left_alloc_adjusted)) {
		// can consolidate left!
		assert(m_freed_allocations[left_alloc_adjusted] == left_alloc_size);

		left_alloc_size += current_alloc_size;

		m_freed_allocations.erase(left_alloc_adjusted);

		return { {left_alloc_adjusted, left_alloc_size} };
	}
	else {
		return {};
	}
}

std::optional<std::pair<uintptr_t, uintptr_t>> garbo::GarbageCollector::consolidate_right_if_needed(uintptr_t* current_alloc, uintptr_t current_alloc_size) {
	uintptr_t right_ptr = reinterpret_cast<uintptr_t>(current_alloc) + current_alloc_size;
	uintptr_t right_ptr_adjusted = right_ptr;

	if (right_ptr > reinterpret_cast<uintptr_t>(m_memory_arena_back + sizeof(uintptr_t))) {
		return {};
	}

	if (m_freed_allocations.contains(right_ptr_adjusted)) {
		// can consolidate right!
		uintptr_t right_allocation_size = m_freed_allocations.at(right_ptr_adjusted);

		m_freed_allocations.erase(right_ptr_adjusted);

		std::pair<uintptr_t, uintptr_t> new_allocation {reinterpret_cast<uintptr_t>(current_alloc), current_alloc_size + right_allocation_size};
		uintptr_t *new_allocation_ptr = reinterpret_cast<uintptr_t*>(new_allocation.first);
		uintptr_t allocation_info = new_allocation_ptr[(new_allocation.second / sizeof(uintptr_t)) - 1];

		// check sanity bit of old allocation, just to be safe
		assert(allocation_info >> 63 == 1);

		return new_allocation;
	}
	else {
		return {};
	}
}

void garbo::GarbageCollector::sweep(uintptr_t ptr_to_sweep) {
	if (!m_allocations.contains(ptr_to_sweep)) {
		return;
	}

	std::pair<uintptr_t, uintptr_t> current_allocation{ ptr_to_sweep, m_allocations.at(ptr_to_sweep) };

	m_allocations.erase(current_allocation.first);

	std::optional<std::pair<uintptr_t, uintptr_t>> left_consolidated_alloc = consolidate_left_if_needed(reinterpret_cast<uintptr_t*>(current_allocation.first), current_allocation.second);

	if (left_consolidated_alloc.has_value()) {
		current_allocation = left_consolidated_alloc.value();
	}

	std::optional<std::pair<uintptr_t, uintptr_t>> right_consolidated_alloc = consolidate_right_if_needed(reinterpret_cast<uintptr_t*>(current_allocation.first), current_allocation.second);

	if (right_consolidated_alloc.has_value()) {
		current_allocation = right_consolidated_alloc.value();
	}

	auto new_free_alloc_ptr = reinterpret_cast<uintptr_t*>(current_allocation.first);

	if (m_zero_memory_on_sweep) {
		std::memset(new_free_alloc_ptr, 0, current_allocation.second);
	}

	// need to update size after consolidation
	new_free_alloc_ptr[(current_allocation.second / sizeof(uintptr_t)) - 1] = (static_cast<uintptr_t>(1) << 63) | current_allocation.second;

	m_freed_allocations.insert(current_allocation);

	//std::cout << "Swept 0x" << std::setfill('0') << std::setw(16) << std::hex << ptr_to_sweep << "\n";
}

size_t garbo::GarbageCollector::get_used_bytes() {
	size_t used_bytes = 0;

	for (std::pair<const uintptr_t, uintptr_t> &allocation : m_allocations) {
		used_bytes += allocation.second;
	}

	return used_bytes;
}

/*void garbo::GarbageCollector::resize_memory_arena(uintptr_t new_arena_size) {
	if (m_arena_size == new_arena_size) {
		// no resizing needed: this is a no-op
		return;
	}
	else if (m_arena_size > new_arena_size) {
		throw std::runtime_error("Shrinking memory arena currently not supported!");
	}

	if (new_arena_size > m_arena_size) {
		if (m_max_arena_size.has_value() && m_max_arena_size.value() < new_arena_size) {
			// can't expand memory arena or add memory arena beyond
			return;
		} else if (m_allocations.empty()) {
			resize_memory_arena();
		}
		else {
			//
		}
	}

	uintptr_t old_arena_size = m_arena_size;
	std::unique_ptr<uintptr_t> old_arena = std::move(m_memory_arena);
	uintptr_t ending_allocation_size = 0;

	m_memory_arena.reset(new uintptr_t[new_arena_size]);
	m_arena_size = new_arena_size;

	if (old_arena == nullptr) {
		ending_allocation_size = new_arena_size;

		m_freed_allocations.insert({ 0x0, new_arena_size });

		std::memset(m_memory_arena.get(), 0, new_arena_size);
	}
	else {
		// TODO(Bobby): defragment memory here as well
	}

	m_memory_arena.get()[(new_arena_size / sizeof(uintptr_t)) - 1] = (static_cast<uintptr_t>(1) << 63) | ending_allocation_size;
	m_memory_arena_back = &m_memory_arena.get()[(new_arena_size / sizeof(uintptr_t)) - 1];
}*/