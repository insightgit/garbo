#include <exception>
#include <filesystem>
#include <fstream>
#include <iostream>

#include "../src/garbage_collector.hpp"

struct LinkedListNode {
	char *test_string;
	size_t string_length;
	LinkedListNode *next;
};

constexpr size_t INTENDED_LINKED_LIST_SIZE = 100;
constexpr uintptr_t UNOCCUPIED_PTR_FLAG = 0xDEADBEEF;
constexpr uintptr_t BYTES_SIZE_LEEWAY = 200;

size_t get_total_expected_size(size_t total_file_size, size_t expected_slice_size) {
	size_t linked_list_node_size = sizeof(LinkedListNode) + sizeof(uintptr_t);

	if (expected_slice_size % sizeof(uintptr_t) != 0) {
		expected_slice_size += expected_slice_size % sizeof(uintptr_t);
	}

	// for heap overhead
	expected_slice_size += sizeof(uintptr_t);

	size_t total_expected_size = (expected_slice_size + linked_list_node_size) * INTENDED_LINKED_LIST_SIZE;

	std::cout << "intended string is " << total_file_size << " bytes long, being broken up into " << INTENDED_LINKED_LIST_SIZE << " strings of about " << expected_slice_size << " bytes size\n";
	std::cout << "expected size per slice is " << expected_slice_size << " + sizeof(LinkedListNode) + sizeof(uintptr_t) (" << linked_list_node_size << ") bytes, giving " << total_expected_size << " total bytes used\n";

	return total_expected_size;
}

std::pair<std::string, size_t> read_file_and_get_slice_size() {
	std::ifstream linked_list_file("../tests/linked_list_lorem_ipsum.txt", std::ios::in);

	const size_t size = std::filesystem::file_size("../tests/linked_list_lorem_ipsum.txt");

	std::pair<std::string, size_t> return_value {std::string(size, '\0'), size / INTENDED_LINKED_LIST_SIZE};

	// Read the whole file into the buffer.
	linked_list_file.read(return_value.first.data(), size);

	assert(size > INTENDED_LINKED_LIST_SIZE);

	return return_value;
}

// returns number of valid char*
size_t populate_linked_list(garbo::GarbageCollector &garbage_collector, LinkedListNode *root_node, const std::pair<std::string, size_t> &file_and_slice_size) {
	const char* original_c_string = file_and_slice_size.first.c_str();

	LinkedListNode *rover = root_node;
	size_t current_char = 0;
	size_t number_of_valid_char_ptr = 0;

	for (int i = 0; INTENDED_LINKED_LIST_SIZE > i; ++i) {
		garbage_collector.force_garbage_search();

		size_t target_size = file_and_slice_size.second;

		if (current_char >= file_and_slice_size.first.size()) {
			target_size = 0;
		}
		else if(i == INTENDED_LINKED_LIST_SIZE - 1) {
			target_size = file_and_slice_size.first.size() - current_char;
		}

		if (target_size == 0) {
			rover->test_string = reinterpret_cast<char*>(UNOCCUPIED_PTR_FLAG);
		}
		else {
			rover->test_string = reinterpret_cast<char*>(garbage_collector.GC_malloc(target_size));

            assert(reinterpret_cast<uintptr_t>(rover) != reinterpret_cast<uintptr_t>(rover->test_string));

			rover->string_length = target_size;

			std::memcpy(rover->test_string, &original_c_string[current_char], target_size);
			current_char += target_size;
			++number_of_valid_char_ptr;
		}

		rover->next = reinterpret_cast<LinkedListNode*>(garbage_collector.GC_malloc(sizeof(LinkedListNode)));

		assert(rover->next != nullptr);

		rover = rover->next;
	}

	return number_of_valid_char_ptr;
}

void allocations_that_will_be_trashed(garbo::GarbageCollector &garbage_collector) {
	// TODO(Bobby): Make odd size allocation test and GC_malloc(0) test
	for (int i = 1; 100 >= i; ++i) {
		garbage_collector.force_garbage_search();

		void *data = garbage_collector.GC_malloc(i);

		memset(data, ~0, i);
	}
}

void verify_contents_of_linked_list(LinkedListNode *root_node, size_t number_of_valid_char_ptr, size_t string_size_bytes, const std::string &expected_string) {
	LinkedListNode *rover = root_node;

	std::string reconstructed_string(string_size_bytes, '\0');

	size_t current_copy_pos = 0;

	for (size_t i = 0; number_of_valid_char_ptr > i; ++i) {
		assert(rover->test_string != nullptr);
		assert(current_copy_pos + rover->string_length <= string_size_bytes);

		std::memcpy(&reconstructed_string.data()[current_copy_pos], rover->test_string, rover->string_length);

		current_copy_pos += rover->string_length;
		rover = rover->next;
	}

	if (reconstructed_string != expected_string) {
		throw std::runtime_error("*** reconstructed string is not intact! there was a memory corruption");
	}
	else {
		std::cout << "*** string is intact!\n";
	}
}

int main() {
	garbo::GarbageCollector garbage_collector{true};
	LinkedListNode *root_node = reinterpret_cast<LinkedListNode*>(garbage_collector.GC_malloc(sizeof(LinkedListNode)));

	std::pair<std::string, size_t> file_and_slice_size = read_file_and_get_slice_size();

	allocations_that_will_be_trashed(garbage_collector);

	garbage_collector.force_garbage_search();

	size_t number_of_valid_char_ptr = populate_linked_list(garbage_collector, root_node, file_and_slice_size);

	garbage_collector.force_garbage_search();

	allocations_that_will_be_trashed(garbage_collector);

	garbage_collector.force_garbage_search();

	verify_contents_of_linked_list(root_node, number_of_valid_char_ptr, file_and_slice_size.first.size() * sizeof(char), file_and_slice_size.first);

	uintptr_t used_bytes = garbage_collector.get_used_bytes();

	std::cout << "garbage collector used bytes " << used_bytes << "\n";

	uint32_t expected_size = get_total_expected_size(std::filesystem::file_size("../tests/linked_list_lorem_ipsum.txt"), file_and_slice_size.second);

	if (used_bytes > expected_size + BYTES_SIZE_LEEWAY) {
		std::cout << "*** used bytes " + std::to_string(used_bytes) + " > expected_size + BYTES_SIZE_LEEWAY (" + std::to_string(BYTES_SIZE_LEEWAY) + ")\n";
	}
	else {
		std::cout << "used bytes is " << used_bytes << "\n";
	}
}