#ifndef GARBO_HPP
#define GARBO_HPP

#include <array>
#include <chrono>
#include <cassert>
#include <cstring>
#include <future>
#include <iostream>
#include <mutex>
#include <optional>
#include <unordered_map>
#include <unordered_set>

#include <thread>

#ifdef _WIN32
#include "platform-specific/windows_platform.hpp"
#else
#include "platform-specific/linux_platform.hpp"
#endif

#include "utils.hpp"

namespace garbo {
	constexpr uintptr_t ARENA_SIZE = 100000;

	class GarbageCollector {
	public:
		GarbageCollector(bool zero_memory_on_sweep, uintptr_t initial_arena_size = ARENA_SIZE, std::optional<uintptr_t> max_arena_size = {}, bool continuous_search_on_dedicated_thread = true);

		virtual ~GarbageCollector() {
			if (m_garbage_searcher_thread.has_value()) {
				m_destructing_object = true;
				m_garbage_searcher_thread.value().join();
			}

			if (m_garbage_collector_thread.has_value()) {
				m_destructing_object = true;
				m_garbage_collector_thread.value().join();
			}
		}

		// TODO(Bobby): implement copy and move constructors

		void force_garbage_search() {
			check_for_garbage();
		}

		size_t get_used_bytes();
		size_t get_garbage_bytes() {
			std::lock_guard<std::mutex> garbage_lock_guard{m_garbage_ptrs_lock};

			for (uintptr_t unallocated_ptr : m_garbage_ptrs) {
				sweep(unallocated_ptr);
			}

			m_garbage_ptrs.clear();
		}

		void* GC_malloc(uintptr_t size);
	private:
		typedef std::pair<uint64_t*, uint64_t*> StackInfo;

		static constexpr size_t GARBAGE_COLLECTOR_THREAD_INTERVAL_MILLIS = 0;
		static constexpr size_t HEAP_SMALL_BLOCK_SIZE = 4096;

		struct MemoryArena {
			std::unique_ptr<uintptr_t> ptr;
			uintptr_t size;
		};

		void search_for_ptrs_in_range(uintptr_t* visited_ptr, uintptr_t allocation_size, std::unordered_set<uintptr_t>& visited_set, std::unordered_set<uintptr_t>& unvisited_set,
									  const std::unordered_map<uintptr_t, uintptr_t> &allocations);
		void check_ptr_ranges(std::unordered_set<uintptr_t> &visited_set, std::unordered_set<uintptr_t> &unvisited_set, const garbo::RegState &reg_state, 
							  const StackInfo &thread_stack_info, const std::unordered_map<uintptr_t, uintptr_t> &allocations);

		void check_for_garbage();

		std::optional<std::pair<uintptr_t, uintptr_t>> consolidate_left_if_needed(uintptr_t* current_alloc, uintptr_t current_alloc_size);
		std::optional<std::pair<uintptr_t, uintptr_t>> consolidate_right_if_needed(uintptr_t* current_alloc, uintptr_t current_alloc_size);

		void sweep(uintptr_t ptr_to_sweep);
		//void resize_memory_arena(uintptr_t new_arena_size);

		std::optional<uintptr_t*> mark_and_use_free_allocation(const std::pair<uintptr_t, uintptr_t> &free_allocation, size_t size_plus_overhead, size_t size);

		void find_garbage(const std::unordered_map<uintptr_t, uintptr_t> &allocations);

		std::mutex m_garbage_collector_lock;

		std::optional<uintptr_t> m_max_arena_size;

		std::unordered_set<uintptr_t> m_memory_arenas;

		std::vector<MemoryArena> m_small_memory_arenas;
		std::unordered_map<uintptr_t, MemoryArena> m_large_memory_arenas; // TODO(Bobby): consider formalizing the possible sizes of these memory arenas
		uintptr_t* m_memory_arena_back; // raw ptr because it doesn't manage its own mem
		uintptr_t m_total_arena_size = 0;
		bool m_zero_memory_on_sweep;

		// TODO(Bobby): Eventually move this inside the memory arena?
		std::unordered_map<uintptr_t, uintptr_t> m_allocations;
		std::unordered_map<uintptr_t, uintptr_t> m_freed_allocations;

		bool m_destructing_object = false;

		std::unordered_set<uintptr_t> m_garbage_ptrs;
		std::mutex m_garbage_ptrs_lock;
		std::optional<std::thread> m_garbage_searcher_thread;
		std::optional<std::thread> m_garbage_collector_thread;
	};
}

#endif