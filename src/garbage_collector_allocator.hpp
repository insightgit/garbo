#include <memory>

#include "garbage_collector.hpp"

namespace garbo {
    template<class T>
    class GCAllocator
    {
    public:
        typedef T value_type;

        GCAllocator(const std::shared_ptr<GarbageCollector>& garbage_collector) : m_garbage_collector(garbage_collector) {}

        template<class U>
        constexpr GCAllocator(const GCAllocator <U>&) noexcept {}

        [[nodiscard]] T* allocate(std::size_t size)
        {
            if (size > std::numeric_limits<std::size_t>::max() / sizeof(T))
                throw std::bad_array_new_length();

            if (auto p = static_cast<T*>(m_garbage_collector->GC_malloc(size)))
            {
                return p;
            }

            throw std::bad_alloc();
        }

        void deallocate(T* p, std::size_t n) noexcept
        {
            // TODO(Bobby): no-op because its a GC
        }
    private:
        std::shared_ptr<GarbageCollector> m_garbage_collector;
    };
}
