#include <exception>
#include <iostream>

#include "../src/garbage_collector.hpp"

int main() {
	garbo::GarbageCollector garbage_collector{true};
	const char *test_message = "hey now\0";
	const char *overwritten_message = "overwri\0";
	char *saved_message;

	for (int i = 0; 100 > i; ++i) {
		char *test_string = reinterpret_cast<char*>(garbage_collector.GC_malloc(8));

		garbage_collector.force_garbage_search();

		if (test_string == nullptr) {
			throw std::runtime_error("*** test string is null! alloc failed!");
		}

		if (i == 0) {
			saved_message = test_string;
			strcpy(test_string, test_message);
		}
		else {
			strcpy(test_string, overwritten_message);
		}
	}

	garbage_collector.force_garbage_search();

	if (strncmp(saved_message, test_message, 8) == 0) {
		std::cout << "*** saved message is intact!\n";
	}
	else {
		throw std::runtime_error("*** saved message is not intact! there was a memory corruption!");
	}

	std::cout << "garbage collector used bytes " << garbage_collector.get_used_bytes() << "\n";
}