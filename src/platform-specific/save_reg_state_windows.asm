code
public _save_reg_state
_save_reg_state proc
	mov 8[rcx], rbx
	mov 16[rcx], rdx
	mov 24[rcx], rsi
	mov 32[rcx], rdi
	mov 40[rcx], rbp
	mov 48[rcx], rsp
	mov 56[rcx], r8
	mov 64[rcx], r9
	mov 72[rcx], r10
	mov 80[rcx], r11
	mov 88[rcx], r12
	mov 96[rcx], r13
	mov 104[rcx], r14
	mov 112[rcx], r15
	ret
	_save_reg_state endp
end