#include <exception>
#include <iostream>

#include "../src/garbage_collector.hpp"

void big_small_allocations_test() {
	garbo::GarbageCollector garbage_collector{true};
	const char* test_message = "hey now\0";
	const char* overwritten_message = "overwri\0";
	char* saved_message;

	char* zero_allocation = reinterpret_cast<char*>(garbage_collector.GC_malloc(0));

	if (zero_allocation != nullptr) {
		throw std::runtime_error("*** zero byte allocation didn't result in a nullptr!");
	}

	char *big_allocation = reinterpret_cast<char*>(garbage_collector.GC_malloc(garbo::ARENA_SIZE * 2));
	char *test_allocation = reinterpret_cast<char*>(garbage_collector.GC_malloc(5));

	if (big_allocation == nullptr || test_allocation == nullptr) {
		throw std::runtime_error("*** big and/or test allocation gave a nullptr");
	}

	std::cout << "garbage collector used bytes " << garbage_collector.get_used_bytes() << "\n";
}

void fragmentation_test() {
	garbo::GarbageCollector garbage_collector{true, garbo::ARENA_SIZE, garbo::ARENA_SIZE};

	{
		std::array<uintptr_t*, 5> pointers_ring_buffer;
		size_t ring_buffer_pos = 0;

		for (int i = 0; garbo::ARENA_SIZE > i; i += sizeof(uintptr_t)) {
			garbage_collector.force_garbage_search();

			pointers_ring_buffer[ring_buffer_pos] = reinterpret_cast<uintptr_t*>(garbage_collector.GC_malloc(sizeof(uintptr_t)));

			if (pointers_ring_buffer[ring_buffer_pos] == nullptr) {
				throw std::runtime_error("*** fragmentation step of the fragmentation test got a nullptr");
			}

			*pointers_ring_buffer[ring_buffer_pos] = 0xdeadbeef;

			if (i >= pointers_ring_buffer.size()) {
				for (uintptr_t* ring_buffer_ptr : pointers_ring_buffer) {
					if (*ring_buffer_ptr != 0xdeadbeef) {
						throw std::runtime_error("*** pointer ring buffer is not 0xdeadbeef and is thus not consistent");
					}
				}
			}

			++ring_buffer_pos;
		}
	}

	garbage_collector.force_garbage_search();


	void *big_allocation = garbage_collector.GC_malloc(garbo::ARENA_SIZE - (sizeof(uintptr_t) * 10));

	if (big_allocation == nullptr) {
		throw std::runtime_error("*** couldn't allocate a big allocation after fragmentation. defragmentation is likely not working.");
	}
}

int main() {
	big_small_allocations_test();
	fragmentation_test();

	std::cout << "*** all ok!\n";
}